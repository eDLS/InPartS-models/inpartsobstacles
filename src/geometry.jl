# TODO: find a more suitable place to put this.
using StaticArrays, LinearAlgebra

"""
    LineSegment{N, T<:AbstractFloat}(p::SV{T}; δ::SV{T})
Representation of an N-dimensional line segment.

This type is designed for the [`closestpoints`](@ref) algorithm. Only the first
end point `p` and the difference vector `δ` are stored, the other end point can
be computed as `q`= p + δ`.

For convenience, the methods [`Base.first(::LineSegment2D)`](@ref) and
[`Base.last(::LineSegment2D)`](@ref) were implemented to return `p` and `q`
respectively. [`Base.length(::LineSegment2D) returns the length of `δ`.

`LineSegment2D` are callable, for a line segment `l` and a number `s`, the syntax
`l(s)` returns the point defined by `l.p + s*l.δ`.
"""
Base.@kwdef struct LineSegment{N, T<:AbstractFloat}
    p::SVector{N,T}
    eφ::SVector{N,T}
    l::T
end

@genexport LineSegment

# legacy compat
const LineSegment2D = LineSegment{2}
const LineSegment3D = LineSegment{3}

"""
    LineSegment2D(center::SV{T}; orientation::T, length::T)
Constructs a [`LineSegment2D{T}`](@ref) using center, angle to the horiziontal
and length of the segment
"""
function LineSegment2D(center::SVector{2, T}; orientation::T, length::T) where {T<:AbstractFloat}
    eφ = unitvector(orientation)
    return LineSegment2D{T}(center - 0.5length*eφ, eφ, length)
end

"""
    LineSegment3D(center::SV{T}; orientation::SV{T}, length::T)
Constructs a [`LineSegment3D{T}`](@ref) using center, orientation vector
and length of the segment
"""
function LineSegment3D(center::SVector{3, T}; orientation::SVector{3, T}, length::T) where {T<:AbstractFloat}
    return LineSegment3D{T}(center - 0.5length*orientation, orientation, length)
end

"""
    (l::LineSegment{N, T})(s::Real) → SVector{N, T}
Returns the point defined by `l.p + T(s)*l.l*l.eφ`.
"""
(l::LineSegment{N, T})(s::Real) where {N, T <: AbstractFloat} = l.p + T(s)*l.l*l.eφ

"""
    Base.first(l::LineSegment2D{T}) → SV{T}
Returns the first endpoint of the [`LineSegment2D`](@ref)
"""
Base.first(l::LineSegment) = l.p

"""
    Base.last(l::LineSegment2D{T}) → SV{T}
Returns the second endpoint of the [`LineSegment2D`](@ref)
"""
Base.last(l::LineSegment) = l.p + l.l*l.eφ

"""
    Base.length(l::LineSegment2D{T}) → T
Returns the length of the [`LineSegment2D`](@ref)
"""
Base.length(l::LineSegment) = l.l



## CLOSEST POINTS ##############################################################

"""
    closestpoints(a::LineSegment{T}, b::LineSegment{T}, w::World; kwargs...) → s::T , t::T
Computes the closest points of two [`LineSegment`](@ref) [^Ericson2004], using the topology of `w`.
Returns the 1-dimensional line parameters `s` and `t`, use `a(s)` and `b(t)` to get the
closest points.

!!! warning

    Be careful when applying this algorithm in periodic systems — it will not produce correct results
    for some cases, especially whenever there are length scales exceding half the domain size involved.
    This is because in periodic systems, the orthogonal projection of a point onto a line segment is not
    unique, which this algorithm does not account for.
    However, it works fine as long as the line segments and distances are small compared to the
    domain, so e.g. short-range interactions between rods on reasonably big domains are ok.

## Optional parameters

* `asquare`, `bsquare` can be used to provide precomputed squared lengths for
     the line segments
* `tol`: lines with a length below `tol` will be treated as points. Defaults to
    `eps(T)`.

[^Ericson2004]: C. Ericson, Real-Time Collision Detection, 1 ed. (CRC Press, 2004).
"""
function closestpoints(a::LineSegment{N, T}, b::LineSegment{N,T}, w; asquare::T = a.l^2,
                       bsquare::T = b.l^2, tol = eps(T)) where {N, T <: AbstractFloat}

    # precompute some stuff
    adelta = a.l * a.eφ
    bdelta = b.l * b.eφ
    baseδ = (a.p - b.p) % w
    baseδ_b = bdelta ⋅ baseδ

    if asquare < tol
        if bsquare < tol
            # both lines degenerate to a point
            return zero(T), zero(T)
        else
            # a degenerates to a point
            return zero(T), clamp(baseδ_b/bsquare, zero(T), one(T))
        end
    else
        baseδ_a = adelta ⋅ baseδ

        if bsquare < tol
            # b degenerates to a point
            return clamp(-baseδ_a/asquare, zero(T), one(T)), zero(T)
        end

        # nondegenerate case starts here!
        parallel = adelta ⋅ bdelta
        denom = asquare*bsquare - parallel*parallel

        if denom < tol
            # pick arbitrary point if lines are parallel
            #TODO: make this a bit more realistic (e.g. halfway through the overlap)
            s = zero(T)
        else
            # pick closest point on line a to line b, clamped to the segment a
            s = clamp((parallel*baseδ_b - baseδ_a*bsquare)/denom, zero(T), one(T))
        end

        # compute closest point on line b to a(s)
        t_nom = (parallel*s + baseδ_b)

        # clamp to segment & recompute s if necessary
        if t_nom < 0
            t = zero(T)
            s = clamp(-baseδ_a/asquare, zero(T), one(T))
        elseif t_nom > bsquare
            t = one(T)
            s = clamp((parallel-baseδ_a)/asquare, zero(T), one(T))
        else
            t = t_nom/bsquare
        end

        return s, t
    end
end

"""
    closestpoint(a::LineSegment, b::SVector, w; asquare, tol) → `s::T`
Computes the closest points to point `b` on  [`LineSegment`](@ref) `a` using the topology of `w`.
Returns the 1-dimensional line parameter `s`, use `a(s)` and
closest points.

!!! warning

    Be careful when applying this algorithm in periodic systems — it will not produce correct results
    for some cases, especially whenever there are length scales exceding half the domain size involved.
    This is because in periodic systems, the orthogonal projection of a point onto a line segment is not
    unique, which this algorithm does not account for.
    However, it works fine as long as the line segments and distances are small compared to the
    domain, so e.g. short-range interactions between rods on reasonably big domains are ok.

## Optional parameters

* `asquare` can be used to provide precomputed squared lengths for
    the line segment
* `tol`: lines with a length below `tol` will be treated as points. Defaults to
    `eps(T)`.
"""
function closestpoint(a::LineSegment{N, T}, b::SVector{N, T}, w; asquare = a.l^2, tol = eps(T)) where {N, T}
    # if the LineSegment is degenerate, the result is trivial
    if asquare < tol
        return zero(T)
    end

    δ = (b - a.p) % w
    return clamp((δ ⋅ a.eφ)/a.l, zero(T), one(T))
end
