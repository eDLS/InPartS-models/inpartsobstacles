export Absorbing, Reflecting

tostring(so::AbstractObstacle) = string(so)

@enum ObstacleType begin
    Absorbing
    Reflecting
end
