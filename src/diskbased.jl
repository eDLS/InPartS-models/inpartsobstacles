using InPartS: fold_back, clamptobbox

export Disk

############################################################################################
#                                           Disk                                           #
############################################################################################
Base.@kwdef struct Disk{N,OT} <: InPartS.AbstractObstacle{N}
    pos::SVector{N,Float64}
    radius::Float64
    inverted::Bool = true
end
InPartS.@genexport(Disk)

Disk(pos, radius) = Disk(pos, radius, false)

function InPartS.getpolygon(so::Disk{2,OT}; res::Int=200, growby::Float64=0.0, kwargs...) where {OT}
    # To make this truly a convex hull need to grow by an additional
    δ = (so.radius + growby) * (1 / cos(π / res) - 1)
    radius = so.radius + (growby + δ) * (OT == Reflecting)
    points = Array{Float64,2}(undef, 2res, 2)

    for n = 1:2res
        s, c = sincos(2π * (n / 2res))
        points[n, :] = so.pos + radius * SVector{2,Float64}(c, s)
    end

    points
end

# Need both definitions to avoid method ambiguity
InPartS.relevant_boxes(so::Disk{2}, bg::InPartS.BoxGrid, w::Domain2D; kwargs...) =
    _relevant_boxes_ndisk(so.pos, so.radius * ifelse(so.inverted, -1, 1), bg, w)
InPartS.relevant_boxes(so::Disk{3}, bg::InPartS.BoxGrid, w::Domain3D; kwargs...) =
    _relevant_boxes_ndisk(so.pos, so.radius * ifelse(so.inverted, -1, 1), bg, w)

function _relevant_boxes_ndisk(sopos::SVector{N,Float64}, soradius, bg, w) where {N}
    # if radius is negative (concave/inverted case):
    # additonal safety factor corresponding to the space diagonal of a box
    # to compensate for the choice of closest point below
    intrange = minimum(bg.size) / 2 + soradius + (soradius < 0 ? norm(bg.size) : 0.0)

    indices = CartesianIndex{N}[]
    # index of the box layer one interaction range away from the wall
    for i ∈ CartesianIndices(bg.boxes)
        boxmins = fold_back(SVector(@.((i.I - 1) * bg.size)), w)
        boxmaxs = fold_back(SVector(@.(i.I * bg.size)), w)
        # FIXME: this is the worst possible choice for inverted obstacles
        # (although it should be compensated for by the decreased |intrange| above)
        cp = clamptobbox(sopos, boxmins, boxmaxs, w)
        if sign(soradius) * norm((sopos .- cp) % w) ≤ intrange
            push!(indices, i)
        end
    end

    indices
end

@forcedef function InPartS.obstacleforces!(so::Disk{N,Absorbing}, p::AbstractParticle, sim::Simulation) where {N}
    δ = p.centerpos - so.pos
    δ² = dot(δ, δ)
    if (so.inverted && δ² > so.radius^2) || (!so.inverted && δ² < so.radius^2)
        rmparticle!(sim, p.id)
    end
end
