export InfiniteWall, RightWall, LeftWall, TopWall, BottomWall, makewall, ngonwalls
export FiniteWall, FiniteWall3D
##########################################################################
#%%                          Infinite Wall
########################################################################

"""
    InfiniteWall{N, OT <: ObstacleType}(support::SVector{N, Float64}, normal::SVector{N, Float64})

Infinite wall obstacle type with arbitrary normal vector.
The normal vector points out of the wall and it must be normalized to unit length.
Type parameter `N` refers the dimension of the system,
`OT` is the obstacle type, which can be either `Absorbing` or `Reflecting`.

Constructors:

    InfiniteWall(support::AbstractVector, φ::Float64)
where φ gives the direction of the wall normal in radians.

    makewall(T::Type{<:InfiniteWall{2}}, p1::SVector{2, Float64}, p2::SVector{2, Float64})=
    makewall(T::Type{<:InfiniteWall{3}}, p1::SVector{3, Float64}, p2::SVector{3, Float64}, p3::SVector{3, Float64}) =
for constructing walls from two or three points.

A default implementation for particle interaction is provided for `Absorbing` walls.
Interactions with reflecting walls can be added by defining:

```julia
    function InPartS.obstacleforces!(so::InfiniteWall{DIMENSION, Reflecting}, p::YourParticleType, sim::Simulation)
        # ...
    end
```
"""
Base.@kwdef struct InfiniteWall{N,OT} <: InPartS.AbstractObstacle{N}
    support::SVector{N,Float64}
    normal::SVector{N,Float64}
end
InPartS.@genexport InfiniteWall

RightWall(pos::Real) = InfiniteWall{2,Reflecting}(SA[pos, 0.0], SA[-1.0, 0.0])
LeftWall(pos::Real) = InfiniteWall{2,Reflecting}(SA[pos, 0.0], SA[1.0, 0.0])
TopWall(pos::Real) = InfiniteWall{2,Reflecting}(SA[0, pos], SA[0.0, -1.0])
BottomWall(pos::Real) = InfiniteWall{2,Reflecting}(SA[0, pos], SA[0.0, 1.0])

InfiniteWall(p::AbstractVector, φ::Real) = InfiniteWall{2,Reflecting}(p, SA[cos(φ), sin(φ)])

makewall(T::Type{<:InfiniteWall{2}}, p1::SVector{2,Float64}, p2::SVector{2,Float64}) =
    T(support=p1, normal=normalize(SA[1.0, -1.0] .* reverse(p2 - p1)))

# normal follows right-hand rule
makewall(T::Type{<:InfiniteWall{3}}, p1::SVector{3,Float64}, p2::SVector{3,Float64}, p3::SVector{3,Float64}) =
    T(support=p1, normal=normalize((p2 - p1) × (p3 - p1)))


function InPartS.getpolygon(so::InfiniteWall{2}; infval::Float64=1e3, growby::Float64=0.0, kwargs...)
    # find inf intersection
    normal = so.normal
    tangent = SA[so.normal[2], -so.normal[1]]

    p1 = so.support + infval * tangent + growby * tangent + growby * normal
    p2 = p1 - infval * so.normal - 2 * growby * normal
    p4 = so.support - infval * tangent - growby * tangent + growby * normal
    p3 = p4 - infval * so.normal - 2 * growby * normal


    return collect([p1'; p2'; p3'; p4'])
end


InPartS.relevant_boxes(so::InfiniteWall{2,Absorbing}, bg::InPartS.BoxGrid, w::AbstractDomain{2}; kwargs...) = _ifwa_relevant_boxes(so, bg, w; kwargs...)
InPartS.relevant_boxes(so::InfiniteWall{3,Absorbing}, bg::InPartS.BoxGrid, w::AbstractDomain{3}; kwargs...) = _ifwa_relevant_boxes(so, bg, w; kwargs...)

function _ifwa_relevant_boxes(so::InfiniteWall{N,Absorbing}, bg::InPartS.BoxGrid, w::AbstractDomain{N}; kwargs...) where {N}
    if any(isperiodic(w))
        # give up
        @warn "Using InfiniteWalls in periodic systems is not recommended, proceed at your own caution"
        return CartesianIndices(bg.boxes)
    end

    # very crude method: shift the plane by interactionrange + box half diagonal
    # then find out which boxe centres are behind the shifted plane
    planeshift = (minimum(bg.size) + sum(x -> x^2, bg.size)) / 2
    p1_prime = so.support + planeshift * so.normal

    indices = CartesianIndex{N}[]

    for i ∈ CartesianIndices(bg.boxes)
        boxcentre = fold_back(SVector(@.((i.I - 0.5) * bg.size)), w)
        if ((boxcentre - p1_prime) ⋅ so.normal) ≤ 0
            push!(indices, i)
        end
    end

    return indices
end

@forcedef function InPartS.obstacleforces!(so::InfiniteWall{N,Absorbing}, p::AbstractParticle, sim::Simulation) where {N}
    δ = dot(p.centerpos - so.support, so.normal)
    if δ < 0
        rmparticle!(sim, p.id)
    end
end

############################################################################################
#                        Polygonal domain boundary constructor                             #
############################################################################################



"""
    ngonwalls(n, walltype = InfiniteWall{2, Absorbing}; kwargs...)
Construct a regular polygonal domain with `n` corners and `walltype` walls.
The size can be provided as keyword arguments by giving a the radius `r` of the circumcircle
`r` or the area `A` (of the polygon) plus an additional `wallsize=0.05` margin.
"""
function ngonwalls(n, walltype=InfiniteWall{2,Absorbing}; kwargs...)

    points, lims = pointsymoffset(ngonpoints(n; kwargs...))
    walls = walltype[]

    for i ∈ 1:n
        push!(walls, makewall(walltype, points[i], points[i+1]))
    end

    return lims, walls
end

function pointsymoffset(points)
    extx = extrema(first, points)
    exty = extrema(last, points)

    dx = max(abs.(extx)...)
    dy = max(abs.(exty)...)

    return (points .+ Ref(SA[dx, dy]), (2dx, 2dy))
end

ngonradius(n; A=1.0) = sqrt(2A / (n * sin(2π / n)))
ngonpoints(n; A=1.0, wallsize=0.05, r=ngonradius(n; A)) = [(r + wallsize) * SA[sincos(2π * (i + (1 - n % 2) / 2) / n)...] for i ∈ 0:n]


Base.@kwdef struct FiniteWall{OT} <: InPartS.AbstractObstacle{2}
    p::SVector{2,Float64}
    q::SVector{2,Float64}
    normal::SVector{2,Float64}
end
InPartS.@genexport FiniteWall


function InPartS.getpolygon(so::FiniteWall; thickness::Float64=3.0, infval::Float64=1e3, growby::Float64=0.0, kwargs...)
    p1 = so.p + so.normal*growby
    p2 = so.q + so.normal*growby
    p3 = so.q - so.normal*max(thickness, growby)
    p4 = so.p - so.normal*max(thickness, growby)

    return collect([p1'; p2'; p3'; p4'])
end

@forcedef function InPartS.obstacleforces!(so::FiniteWall{Absorbing}, p::AbstractParticle, sim::Simulation)
    δv = p.centerpos - so.p
    t = so.q - so.p
    δ = dot(δv, so.normal)

    δvp = δv - δ * so.normal
    δ2 = dot(δvp, t)
    if δ < 0 && (0.0 ≤ δ2 ≤ t⋅t)
        rmparticle!(sim, p.id)
    end
end


##

Base.@kwdef struct FiniteWall3D{OT} <: InPartS.AbstractObstacle{3}
    a::SVector{3,Float64}
    b::SVector{3,Float64}
    c::SVector{3,Float64}
    normal::SVector{3,Float64}
end
InPartS.@genexport FiniteWall3D

makewall(T::Type{<:FiniteWall3D}, p1::SVector{3,Float64}, p2::SVector{3,Float64}, p3::SVector{3,Float64}) =
    T(a=p1, b=p2, c=p3, normal=normalize((p2 - p1) × (p3 - p1)))


# function InPartS.getpolygon(so::FiniteWall3D; thickness::Float64=3.0, infval::Float64=1e3, growby::Float64=0.0, kwargs...)
#     p1 = so.p
#     p2 = so.q
#     p3 = so.q - so.normal*thickness
#     p4 = so.q - so.normal*thickness

#     return collect([p1'; p2'; p3'; p4'])
# end

@forcedef function InPartS.obstacleforces!(so::FiniteWall3D{Absorbing}, p::AbstractParticle, sim::Simulation)
    # a as reference points
    v1 = so.b - so.a
    v2 = so.c - so.a
    w1 = so.c - so.b
    w2 = so.a - so.b

    vq = p.centerpos - so.a

    δ = dot(vq, so.normal) # in front or behind surface
    δ > 0 && return

    # project onto plane
    vq = vq - δ * so.normal

    # should be inside triangle
    (vq × v1) ⋅ (vq × v2) > 0 && return

    vq = p.centerpos - so.b
    δ = dot(vq, so.normal) # in front or behind surface
    (vq × w1) ⋅ (vq × w2) > 0 && return

    rmparticle!(sim, p.id)
end

function InPartS.relevant_boxes(so::FiniteWall3D{Reflecting}, bg::InPartS.BoxGrid, w::Domain3D{Boundary,Boundary,Boundary}; kwargs...)
    # very crude method: shift the plane by interactionrange + box half diagonal
    # then find out which box centres are behind the shifted plane
    planeshift = (minimum(bg.size) + norm(bg.size))/2#sum(x -> x^2, bg.size)) / 2
    support = so.a# + planeshift * so.normal

    indices = CartesianIndex{3}[]

    for i ∈ CartesianIndices(bg.boxes)
        boxcentre = fold_back(SVector(@.((i.I - 0.5) * bg.size)), w)
        if (-planeshift-so.thickness) ≤ ((boxcentre - support) ⋅ so.normal) ≤ planeshift
            (boxcentre-so.a)⋅(boxcentre-so.a) > planeshift^2 &&
            (boxcentre-so.b)⋅(boxcentre-so.b) > planeshift^2 &&
            (boxcentre-so.b)⋅(boxcentre-so.b) > planeshift^2 && continue

            push!(indices, i)
        end
    end


    return indices
    #return vec(CartesianIndices(bg.boxes))
end
