export RodObstacle, ArcWall
###########################################################################################
#                            Finite Wall ( Rod Obstacle )                                 #
###########################################################################################

"""
    RodObstacle(p1, p2, radius; inverted = false) <: AbstractObstacle
Construct a spherocylindrical rod between points `p1` and `p2`
with thickness `2*radius`. With `inverted = true`, the obstacle
is the entire space *except* the spherocylinder.
"""
Base.@kwdef struct RodObstacle{D} <: AbstractObstacle{D}
    backbone::LineSegment{D,Float64}
    radius::Float64
    inverted::Bool = false

end
# Legacy positional constructurs (without inverted)
RodObstacle(backbone, radius) = RodObstacle(backbone, radius, false)
RodObstacle{D}(backbone, radius) where {D} = RodObstacle{D}(backbone, radius, false)

#const FiniteWall = RodObstacle{2}

# this is a bit weird for legacy compatibility
@exporttransform RodObstacle backbone [value.p; value.eφ; value.l] begin
    dims = round(Int, (length(data) - 1) ÷ 2)
    LineSegment{dims,Float64}(SVector{dims}(data[1:dims]), SVector{dims}(data[dims+1:2dims]), data[2dims+1])
end

InPartS.@genexport(RodObstacle)

function RodObstacle{D}(p1::SVector{D,Float64}, p2::SVector{D,Float64}, radius; inverted=false) where {D}
    l = norm(p2 .- p1)
    eφ = l > 0.0 ? SVector{D,Float64}(((p2 .- p1) ./ l)...) : SVector{D,Float64}(1.0, zeros(Float64, D - 1)...)
    return RodObstacle{D}(LineSegment{D,Float64}(p1, eφ, l), radius, inverted)
end

function InPartS.getpolygon(so::RodObstacle{2}; res::Int=25, growby::Float64=0.0)
    # To make this truly a convex hull need to grow by an additional
    δ = (so.radius + growby) * (1 / cos(π / res) - 1)
    radius = so.radius + growby + δ
    α = π / 2
    rdhat = @. radius * so.backbone.eφ#unitvector(cell.φ)

    function rRdhat(θ)
        sθ, cθ = sincos(θ)
        return SA[cθ*rdhat[1]-sθ*rdhat[2],
            sθ*rdhat[1]+cθ*rdhat[2]]
    end

    points = Array{Float64}(undef, 2res, 2)
    for (i, θ) in enumerate(range(α, stop=2π - α, length=res))
        points[i, :] = first(so.backbone) + rRdhat(θ)
    end
    for (i, θ) in enumerate(range(α, stop=2π - α, length=res))
        points[i+res, :] = last(so.backbone) - rRdhat(θ)
    end

    return points
end

tostring(fw::RodObstacle) = "RodObstacle(r ≈ [$(join([first(fw.backbone), last(fw.backbone)], ","))], l ≈ $(fw.backbone.l), φ ≈ $(angle(fw.backbone.eφ[1] + im*fw.backbone.eφ[2])), inverted = $(fw.inverted))"

###########################################################################################
#                                         Arc Wall                                        #
###########################################################################################

# TODO: 3D‽
"""
    ArcWall(pos, arcradius, radius, ϑmin, ϑmax) <: AbstractObstacle
Obstacle that forms an arc segment with rounded caps.
Currently 2D only.
"""
Base.@kwdef struct ArcWall <: AbstractObstacle{2}
    pos::SVector{2,Float64}
    arcradius::Float64
    radius::Float64
    ϑmin::Float64 # always -π < ϑmin < π
    ϑmax::Float64 # always ϑmin < ϑmax < 2π and ϑmax < ϑmin + 2π

    function ArcWall(pos, arcradius, radius, ϑmin, ϑmax)
        ϑmin = mod2pi(ϑmin)
        ϑmax = mod2pi(ϑmax)
        ϑmax += (ϑmax < ϑmin) ? 2π : 0
        new(pos, arcradius, radius, ϑmin, ϑmax)
    end
end

@genexport ArcWall
function InPartS.getpolygon(so::ArcWall; res::Int=50, growby::Float64=0.0, kwargs...)
    # start at ϑmin in inner arc
    δϑ = (so.ϑmax - so.ϑmin) / res

    # To make this truly a convex hull need to grow by an additional
    δ = max((so.radius + growby) * (1 / cos(π / res) - 1),
        (so.arcradius + growby) * (1 / cos(δϑ) - 1))

    radius = so.radius + growby + δ
    points = Array{Float64,2}(undef, 4res, 2)

    for n = 1:res
        ϑ = so.ϑmin + n * δϑ
        s, c = sincos(ϑ)
        points[n, :] = so.pos + (so.arcradius - radius) * SV{Float64}(c, s)
    end
    # Now do the cap
    # ϑ stays the same but φ starts at ϑ+π and goes to ϑ
    δφ = π / res
    φmax = so.ϑmax + π

    sϑ, cϑ = sincos(so.ϑmax)
    for n = 1:res
        φ = φmax - n * δφ
        sφ, cφ = sincos(φ)
        points[n+res, :] = so.pos + (so.arcradius) * SV{Float64}(cϑ, sϑ) + radius * SV{Float64}(cφ, sφ)
    end

    # Now the outer arc
    for n = 1:res
        ϑ = so.ϑmax - n * δϑ
        s, c = sincos(ϑ)
        points[n+2res, :] = so.pos + (so.arcradius + radius) * SV{Float64}(c, s)
    end

    # Now the other cap
    φmax = so.ϑmin
    sϑ, cϑ = sincos(so.ϑmin)
    for n = 1:res
        φ = φmax - δφ * n
        sφ, cφ = sincos(φ)
        points[n+3res, :] = so.pos + (so.arcradius) * SV{Float64}(cϑ, sϑ) + radius * SV{Float64}(cφ, sφ)
    end
    points
end


############################################################################################
#                               Generate Mushroom Obstacles                                #
############################################################################################

function mushroom_obstacles(; capradius = 100.0, stemradius = 0.4capradius, stemlength = 3capradius/4, wallradius = 0.5, openstem = false)
    maxstemlength = sqrt(capradius^2 + stemradius^2)
    if stemlength > maxstemlength
        @warn "Stem too long, capping to $maxstemlength"
        stemlength = maxstemlength
    end

    offset = SA[capradius, stemlength]

    capdisk = Disk{2, Absorbing}(offset, capradius, true)

    capbottom_l = RodObstacle{2}(
        SA[-capradius,  -wallradius] + offset,
        SA[-stemradius - wallradius, -wallradius] + offset,
        wallradius
    )

    capbottom_r = RodObstacle{2}(
        SA[ stemradius + wallradius, -wallradius] + offset,
        SA[ capradius,  -wallradius] + offset,
        wallradius
    )

    stemside_l = RodObstacle{2}(
        SA[-stemradius - wallradius, -wallradius] + offset,
        SA[-stemradius - wallradius, -stemlength] + offset,
        wallradius
    )

    stemside_r = RodObstacle{2}(
        SA[ stemradius + wallradius, -stemlength] + offset,
        SA[ stemradius + wallradius, -wallradius] + offset,
        wallradius
    )

    obstacles = if (openstem)
        [capdisk, capbottom_l, stemside_l, stemside_r, capbottom_r]
    else
        [capdisk, capbottom_l, stemside_l, BottomWall(0.0), stemside_r, capbottom_r]
    end

    return (; obstacles, domainsize = (2capradius, capradius+stemlength))
end
