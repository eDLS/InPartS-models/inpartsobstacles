# InPartSObstacles

Obstacle definitions for InPartS simulations.

## Basic concepts

This package defines the structures and semantics for obstacles along with basic utilities.
Interaction functions with particle models need to be implemented alongside the respective particle model.

Some obstacle types support a type parameter that specifies whether their interaction is `Absorbing` (i.e. they remove particles on contact) or `Reflecting` (i.e. they excert a force).
These can be used for dispatch when implementing interaction functions.
Obstacles that support the interaction type parameter currently all come with a default interaction function for `Absorbing` that removes cells if their `centerpos` lies within the obstacle (consistent with the behaviour of InPartS boundaries).

## List of Obstacles

| Name | Supported Dimensions | Interaction type parameter | Default implementation for `Absorbing` |  Remarks |
|-|-|-|-|-|
|`InfiniteWall` | 2,3 | ✔ | ✔ |Represents an infinite planar *D*-1 dimensional wall.
|`Disk` | 2,3 | ✔ | ✔ |  Can be used to represent either a solid disk/sphere or a spherical cavity (using the `inverted` parameter).|
| `RodObstacle` | 2, 3 | ✘ | n.a. | Can be used to represent either a solid rod or a rod-shaped cavity (using the `inverted` parameter) |
| `ArcWall` | 2 | ✘ | n. a. | |
|`FiniteWall` | 2 | ✔ | ✔ | Represents a 2D straight-line wall segment between two points. |
|`FiniteWall3D` | 3 | ✔ | ✔ | Represents triangular wall segment between three points in 3D. |



## Included convenience functions and other stuff

The `ngonwalls` function can be used to arrange `InfiniteWalls` in a regular polygonal shape.

The `mushroom_obstacles` function can be used to arrange several RodObstacles and an absorbing disk in a shape reminiscent of a mushroom.

InPartSObstacles includes the `LineSegment` type and the `closestpoints` function based on Ericson's Real-Time Collison Detection.

## Submitting new obstacles

We are always looking to expand our collection of obstacles and are happy to accept your voluntary contribution [^1]

1. Have a look at the existing obstacles (especially the `Disk`, which is the most recent one) and  infer the requirements and coding style
2. Implement your obstacle and open a MR
3. Bribe the maintainers e.g. with cookies or a cake to ensure your MR gets the consideration it deserves.

---
[^1]: if you are our student and we forced you to contribute, that still counts as “voluntary”
