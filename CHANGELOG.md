## v0.1.6
 - added license, set visibility to public (hi everyone!)

## v0.1.5
 - update compat for InPartS

## v0.1.4
 - *bugfix*: removed ambiguity in relevant_boxes for absorbing infinite walls

## v0.1.3
 - fixed getpolygon for FiniteWalls
 - updated compat bounds

## v0.1.2
 - *new obstacle*: FiniteWall, FiniteWall3D
 - added direct box computation for infinite walls
 - nicer readme file

## v0.1.1
 - fixed broken InPartS compat
 - added changelog (hi!)
